package dao;

import java.sql.Connection;
import java.util.List;

import Classes.Club;
/**
 * 
 * @author Maestro
 *
 * @param <T>
 */

public abstract class DAO<T> {
	public static Connection connect= ConnexionPostgreSql.getInstance();
	/**
	 * Creation d'un objet
	 * @param obj
	 */
	public abstract void create(T obj);
	
	/**
	 * Permet d'obtenir un objet
	 * @param clefPrimaire
	 * @return T
	 */
	public abstract T read(String clefPrimaire);
	
	/**
	 * Permet de supprimer un objet
	 * @param obj
	 */
	public abstract void delete(T obj);
	
	/**
	 * Permet de modifier un objet
	 * @param obj
	 */
	public abstract void update(T obj);
	
	/**
	 * Permet d'obtenir une liste d'objet
	 * @param clefPrimaire
	 * @return T
	 */
	public abstract List<T> recupAll();
	
	
}
