package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnexionPostgreSql {
	
			private static Connection connect;


	public static Connection getInstance(){
		if(connect == null){
			try {
				
				connect = DriverManager.getConnection("jdbc:postgresql://postgresql.bts-malraux72.net:5432/t.lehugeur", "t.lehugeur", "P@ssword");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return connect;
		}
}
