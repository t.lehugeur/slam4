package vues;

import java.awt.EventQueue;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import java.awt.BorderLayout;
import javax.swing.JScrollBar;
import javax.swing.JTextArea;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JComboBox;
import javax.swing.JTextField;

import Classes.Club;
import controleur.Controleur_principal;
import dao.ClubDAO;

import javax.swing.ComboBoxEditor;
import javax.swing.JButton;
import javax.swing.JLabel;;

public class MaVue  extends JPanel{

	private JFrame frame;;
	protected Controleur_principal controleur;;
	private JTextField nom;;
	private JTextField prenom;;
	private JTextField date_naissance;
	private JComboBox comboBox;
	
	
	public MaVue(Controleur_principal unControleur) {
		this.controleur=unControleur;;
		// définition de la vue
		setLayout(null);
		setBounds(0, 23, 434, 239);
		initialize();
	}
	

	
	 
	

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();;
		frame.getContentPane().setLayout(null);;
		
		frame.setVisible(true);
		comboBox = new JComboBox();;
		comboBox.setBounds(151, 32, 201, 24);;
		frame.getContentPane().add(comboBox);;
		
		JButton btnAjouter = new JButton("Ajouter");;
		btnAjouter.setBounds(22, 208, 114, 25);;
		frame.getContentPane().add(btnAjouter);;
		
		JButton btnSupprimer = new JButton("Supprimer");;
		btnSupprimer.setBounds(158, 208, 114, 25);;
		frame.getContentPane().add(btnSupprimer);
		;
		JButton btnQuitter = new JButton("Quitter");;
		btnQuitter.setBounds(97, 226, 114, 25);;
		frame.getContentPane().add(btnQuitter);;
		
		JLabel lblChoisirUnClub = new JLabel("Choisir un club :");
		lblChoisirUnClub.setBounds(22, 22, 170, 15);
		frame.getContentPane().add(lblChoisirUnClub);
		
		JList list = new JList();
		list.setBounds(41, 73, 377, 108);
		frame.getContentPane().add(list);
		
		JLabel lblNom = new JLabel("nom :");
		lblNom.setBounds(53, 74, 66, 15);
		frame.getContentPane().add(lblNom);
		
		JLabel lblPrenom = new JLabel("prenom :");
		lblPrenom.setBounds(53, 101, 66, 15);
		frame.getContentPane().add(lblPrenom);
		
		JLabel lblDateNaissance = new JLabel("date naissance :");
		lblDateNaissance.setBounds(53, 134, 66, 15);
		frame.getContentPane().add(lblDateNaissance);
		
		nom = new JTextField();
		nom.setBounds(203, 72, 124, 19);
		frame.getContentPane().add(nom);
		nom.setColumns(10);
		
		prenom = new JTextField();
		prenom.setBounds(203, 99, 124, 19);
		frame.getContentPane().add(prenom);
		prenom.setColumns(10);
		
		date_naissance = new JTextField();
		date_naissance.setBounds(203, 132, 124, 19);
		frame.getContentPane().add(date_naissance);
		date_naissance.setColumns(10);
		
		JButton btnEnregistrer = new JButton("Enregistrer");
		btnEnregistrer.setBounds(151, 156, 114, 25);
		frame.getContentPane().add(btnEnregistrer);
	
	}
	
	public void remplirComboClub(List<Club> lesClubs) {
		comboBox.addItem(new Club());
		for (Club c : lesClubs)
		{
			comboBox.addItem(c);
		}
	}
}

