package controleur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import Classes.Club;
import Classes.Licencie;
import dao.ClubDAO;
import dao.DAO;
import dao.LicencieDAO;
import vues.MaVue;
import vues.Vue_principale;

public class Controleur_principal implements ActionListener {
	
	private MaVue vue;
	private DAO<Club> gestionClub;
	private DAO<Licencie> gestionLicencie;
	private List<Club> lesClubs;
	

	public MaVue getVue() {
		return vue;
	}


	public void setVue(MaVue vue) {
		this.vue = vue;
	}


	public DAO<Club> getGestionClub() {
		return gestionClub;
	}


	public void setGestionClub(DAO<Club> gestionClub) {
		this.gestionClub = gestionClub;
	}


	public DAO<Licencie> getGestionLicencie() {
		return gestionLicencie;
	}


	public void setGestionLicencie(DAO<Licencie> gestionLicencie) {
		this.gestionLicencie = gestionLicencie;
	}


	public List<Club> getLesClubs() {
		return lesClubs;
	}


	public void setLesClubs(List<Club> lesClubs) {
		this.lesClubs = lesClubs;
	}

	
	public Controleur_principal()
	{
	this.gestionClub=new ClubDAO();
	this.gestionLicencie= new LicencieDAO();
	
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		if (arg0.getActionCommand().equals("quitter"))
				{
				Quitter(); // déconnection bdd et arrêt de l’application
				}
				// si on clique sur le bouton enregistrer (un nouveau licencié)
				else if (arg0.getActionCommand().equals("ajouter"))
				{
				// insertion d’un nouveau licencié dans la bdd en récupérant les données de la vue
				// retour à la vue avec mise à jour des licenciés du club dans la zone prévue
					gestionClub.create(new Club());
				}
				// si un autre évènement est à l’origine de l’appe
	}
	
	public void Quitter() {
		
	}
	 
}

