package tests;

import dao.ClubDAO;
import dao.LicencieDAO;
import Classes.Club;
import Classes.Licencie;
import dao.DAO;

public class TestDAO {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		DAO<Licencie> licencie = new LicencieDAO();
		DAO<Club> club = new ClubDAO();
		
		// test de la recherche d'un licencié en fonction de son code
		System.out.println(licencie.read("TAGe").toString());
		
		// test de la recherche d'un club en fonction de son code
		Club unClub = club.read("C1");
		Licencie unLicencie = new Licencie("CACl", "CALVIN","Clémence","17/05/1990", unClub);
		
		// test de l'insertion d'un nouveau licencié
		licencie.create(unLicencie);

	}

}
