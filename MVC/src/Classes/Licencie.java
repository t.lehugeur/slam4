package Classes;

/**
 * 
 * @author Maestro
 *
 */
public class Licencie {

	private String code, nom, prenom, dateNaiss;
	private Club leClub;
	
	/**
	 * constructeur par defaut 
	 */
	public Licencie() {
		code=nom=prenom=dateNaiss="!";
		leClub=new Club();
	}
	
	/**
	 * constructeur avec parametre
	 * @param code
	 * @param nom
	 * @param prenom
	 * @param dateNaiss
	 * @param leClub
	 */
	public Licencie(String code, String nom, String prenom, String dateNaiss, Club leClub) {
		super();
		this.code = code;
		this.nom = nom;
		this.prenom = prenom;
		this.dateNaiss = dateNaiss;
		this.leClub = leClub;
	}
	
	/**
	 * Permet d'obtenir le code du licencie
	 * @return code
	 */
	public String getCode() {
		return code;
	}
	/**
	 * Permet de modifier le code du licencie
	 */
	public void setCode(String code) {
		this.code = code;
	}
	/**
	 * Permet d'obtenir le nom du licencie
	 * @return nom
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * Permet de modifier le nom du licencie
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * Permet d'obtenir le prenom du licencie
	 * @return prenom
	 */
	public String getPrenom() {
		return prenom;
	}
	/**
	 * Permet de modifier le prenom du licencie
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	/**
	 * Permet d'obtenir la date de naissance du licencie
	 * @return dateNaiss
	 */
	public String getDateNaiss() {
		return dateNaiss;
	}
	/**
	 * Permet de modifier la date de naissance du licencie
	 */
	public void setDateNaiss(String dateNaiss) {
		this.dateNaiss = dateNaiss;
	}
	/**
	 * Permet d'obtenir le Club du licencie
	 * @return leClub
	 */
	public Club getLeClub() {
		return leClub;
	}
	/**
	 * Permet de modifier le Club du licencie
	 */
	public void setLeClub(Club leClub) {
		this.leClub = leClub;
	}
	@Override
	public String toString() {
		return "Licencie [code=" + code + ", nom=" + nom + ", prenom=" + prenom + ", dateNaiss=" + dateNaiss
				+ ", leClub=" + leClub + "]";
	}
	
	
}
