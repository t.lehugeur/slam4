package Classes;

/**
 * 
 * @author Maestro
 *
 */

public class Club {
	private String code, nom, nom_president, nom_entraineur;


	
	/**
	 * Constructeur par default
	 */
	public Club() {
		code=nom=nom_president=nom_entraineur="!";
	}

	/**
	 * constructeur avec parametre
	 * @param code
	 * @param nom
	 * @param nom_president
	 * @param nom_entraineur
	 */
	public Club(String code, String nom, String nom_president, String nom_entraineur) {
		super();
		this.code = code;
		this.nom = nom;
		this.nom_president = nom_president;
		this.nom_entraineur = nom_entraineur;
	}

	/**
	 * permet d'obtenir le code du club
	 * @return code
	 */
	public String getCode() {
		return code;
	}
	/**
	 * permet de modifier le code du club
	 */
	public void setCode(String code) {
		this.code = code;
	}
	/**
	 * permet d'obtenir le nom du club
	 * @return nom
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * permet de modifier le nom du club
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * permet d'obtenir le nom du president du club
	 * @return nom_president
	 */
	public String getNom_president() {
		return nom_president;
	}
	/**
	 * permet de modifier le nom du president du club
	 */
	public void setNom_president(String nom_president) {
		this.nom_president = nom_president;
	}
	/**
	 * permet d'obtenir le nom de l'entraineur du club
	 * @return nom_entraineur
	 */
	public String getNom_entraineur() {
		return nom_entraineur;
	}
	/**
	 * permet de modifier le nom de l'entraineur du club
	 */
	public void setNom_entraineur(String nom_entraineur) {
		this.nom_entraineur = nom_entraineur;
	}

	@Override
	public String toString() {
		return "Club [code=" + code + ", nom=" + nom + ", nom_president=" + nom_president + ", nom_entraineur="
				+ nom_entraineur + "]";
	}
	
	
}
