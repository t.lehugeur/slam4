package slam;

import java.io.IOException;

import javax.swing.JOptionPane;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory; 

/**
 * Définition de la vue principale du programme
 * @author geraldine
 * @version 2.0 du 21/11/2019
 */
public class Vue {
	
	// Logger : cet objet permettra d'émettre un message dans la console par exemple
	private static final Log logger = LogFactory.getLog(Vue.class);
	
	// nom logique du fichier XML contenant les utilisateurs
	private String fileNameUsers;

	/**
	 * Constructeur avec en paramètre le nom physique du fichier xml utilisé
	 * @param fileNameUsers : nom du fichier XML
	 */
	public Vue(String fileNameUsers){
		this.fileNameUsers = fileNameUsers;
	}
	
	/**
	 * Entrée principale de l'application - méthode appelée dans le pgme main
	 * Cette méthode est susceptible de lever une exception (devra donc être traitée dans un try / catch à son appel 
	 * 
	 * @throws IOException
	 * 
	 */
	public void run() throws IOException {
		// définition de l'objet qui permettra de récupérer et manipuler les données  
		GestionAjoutUsers gestionAjoutUsers = new GestionAjoutUsers(fileNameUsers);		
		// récupération de la date/heure du jour (utile ici pour les messages Log)
		java.util.Date today = new java.util.Date();
		logger.info("Début exécution : " + today.toString());	
		String choix;
		String menu = "0 : Quitter\n1 : Ajouter un utilisateur\n2 : Lister les utilisateurs\nVotre choix SVP :";
		do {	
			// paramètrage et affichage d'une boite de dialog 
			// la saisie de l'administrateur sera récupérée dans la variable choix
			choix = JOptionPane.showInputDialog(null, menu, "Ajout utilisateur", JOptionPane.QUESTION_MESSAGE);
			
			// cas où on veut ajouter un utilisateur
			if ("1".equals(choix)) {
				String prenom = JOptionPane.showInputDialog("Entrez un prénom");
				String nom = JOptionPane.showInputDialog("Entrez un nom");
				if (prenom != null && !"".equals(prenom) && nom != null
						&& !"".equals(nom)) {
					gestionAjoutUsers.ajoutUser(prenom, nom);
					gestionAjoutUsers.saveToFile();
				}
			} else if ("2".equals(choix)) { // cas où on veut lister les utilisateurs
				JOptionPane.showMessageDialog(null, gestionAjoutUsers.toString());
			}
		} while (!"0".equals(choix) && choix != null);
		today = new java.util.Date();
		logger.info("Fin exécution : " + today.toString());
	}
	
}
