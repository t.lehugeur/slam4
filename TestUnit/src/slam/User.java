package slam;

/**
 * Définition de la classe User
 * @author geraldine
 * @version 2.0 du 21/11/2019
 */
public class User {

	// définition des membres propriétés - en privée
	private String prenom;
	private String nom;
	private String id;
	private String mdp;
	
	// définition des accesseurs / modifieurs
	
	/**
	 * Permet d'obtenir l'identifiant d'un utilisateur 
	 * @return l'id de l'utilisateur - chaîne de caractères
	 */
	public String getId() {
		return this.id;
	}

	// définition des constructeurs
	/**
	 * Constructeur par défaut
	 */
	public User() {
		super();
	}
 
	/**
	 * Constructeur avec apport de valeur aux propriétés par les paramètres
	 * @param prenom 
	 * @param nom
	 * @param id
	 * @param mdp
	 */
	public User(String prenom, String nom, String id, String mdp) {
		this.prenom=prenom;
		this.nom=nom;
		this.id=id;
		this.mdp=mdp;
	}
	
	
	
	public String getMdp() {
		return mdp;
	}

	public void setMdp(String mdp) {
		this.mdp = mdp;
	}

	// définition des membres méthodes
	/**
	 * Méthode complètement redéfinie ici pour pouvoir faire une ligne avec les bonnes balises
	 * et les bonnes valeurs de données dans les balises du fichier xml
	 */
	@Override
	public String toString(){
		StringBuffer res = new StringBuffer();
		res.append("<user>");
		res.append("<prenom>");
		res.append(this.prenom);
		res.append("</prenom>");
		res.append("<nom>");
		res.append(this.nom);
		res.append("</nom>");
		res.append("<id>");
		res.append(this.id);
		res.append("</id>");
		res.append("<mdp>");
		res.append(this.mdp);
		res.append("</mdp>");
		res.append("</user>");
		return res.toString();		
	}

	
}
