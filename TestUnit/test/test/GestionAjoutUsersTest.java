package test;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import junit.framework.TestCase;
import slam.GestionAjoutUsers;
import slam.User;

class GestionAjoutUsersTest extends TestCase{
	private GestionAjoutUsers gau;

	@BeforeEach
	protected void setUp() throws Exception {
	try {
		this.gau = new GestionAjoutUsers("testusers.xml");
	} catch (IOException e) {
	fail("Création de l'OUT impossible !");
	}
	}

	@Test
	public void test2PremiersCarsGenereId() {
		String id = this.gau.genereId("Casimir",  "Martin);");
		assertTrue("Les 2 premiers caractères sont invalides", id.startsWith("cm"));
		//fail("Not yet implemented");
	}
	
	@Test
	public void test2PremiersCarsGenereIdBis() {
		String id = this.gau.genereId("Casimir",  "Martin");
		String premscar = id.substring(0,5);
		assertEquals("Les 2 premiers caractères sont NON VAlIDES", "cmart", premscar);
		//fail("Not yet implemented");
	}
	
	@Test
	public void test2PremiersCarsAjout() {
		User id = this.gau.ajoutUser("Pierre", "Leduc");
		assertTrue("Les 2 premiers caractères sont NON VAlIDES", id.getId().startsWith("pleduc"));
		User id2 = this.gau.ajoutUser("Paul", "Leduc");
	
     //	assertNotEquals("Les 2 id sont différents", id2.getId(),id.getId());
     	assertFalse("Les 2 premiers caractè", id.getId().equals(id2.getId()));
		//fail("Not yet implemented");
	}
	
	@Test
	public void test2PremiersCarsGenereMDP() {
		String id = this.gau.genereMotPass(9);
		int premscar = 8;
		assertEquals("Les 2 premiers caractères sont NON VAlIDES", id.length(), premscar);
		//fail("Not yet implemented");
	}
	@Test
	public void test2PremiersCarsAjoutMDP() {
		User id = this.gau.ajoutUser("Pierre", "Leduc");
		assertTrue("Les 2 premiers caractères sont NON VAlIDES", id.getId().startsWith("pleduc"));
		User id2 = this.gau.ajoutUser("Paul", "Leduc");
	
     //	assertNotEquals("Les 2 id sont différents", id2.getId(),id.getId());
     	assertFalse("Les 2 premiers caractè", id.getId().equals(id2.getId()));
		//fail("Not yet implemented");
	}
}
