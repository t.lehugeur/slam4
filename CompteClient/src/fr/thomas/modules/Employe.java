package fr.thomas.modules;

public class Employe extends Salarie{

    private String banque;

    public Employe(int salaire, String prenom, String nom, String banque) {
        super(salaire, prenom, nom);
        this.banque = banque;
    }

    @Override
    public void vererSalaire() {
        System.out.println("Un chèque du montant de " + getSalaire() + " a été envoyé pour le règlement du salarié");
    }

    @Override
    public String toString() {
        return super.toString() + ", banque: " + banque;
    }

    public String getBanque() {
        return banque;
    }

    public void setBanque(String banque) {
        this.banque = banque;
    }
}
