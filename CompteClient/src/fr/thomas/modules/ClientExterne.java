package fr.thomas.modules;


import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @author geraldine
 *
 */
public class ClientExterne implements Client
{
     // liste des comptes du client
    private List<Compte> lesComptes;
    // nom du client externe
    private String nom;
    //prénom du client externe
    private String prenom;

     // la liste des comptes ne peut être modifiée directement
    public List<Compte> getLesComptes()
    {
         return lesComptes; 
    }

    // accès en lecture au nom
    public String getNom()
   {
       return nom;  
   }

    // accès en lecture au prénom
   public String getPrenom()
   {
       return prenom; 
   }
     //constructeurs par défaut
   public ClientExterne()
   {
       nom = "titi";
       prenom = "toto";
       lesComptes = new ArrayList<Compte>();
   }
    
    // constructeur avec initialisation des attributs de clientExterne avec les valeurs passées en paramètres
   public ClientExterne(String nom, String prenom)
   {
       this.nom = nom;
       this.prenom = prenom;
       lesComptes = new ArrayList<Compte>();
   }
    
   
   // création d'un compte pour un ClientExterne
   @Override
   public void creerCompte()
   {
	   Scanner saisie = new Scanner(System.in);
        
       System.out.println("\n  ===> CREATION DE COMPTE - pour client externe");
       System.out.print("entrer le nom du titulaire : ");
       String nom = saisie.next();
       System.out.print("\nentrer le numéro du compte : ");
       String num = saisie.next();
       System.out.print("entrer le solde : ");
       double solde = saisie.nextDouble();

       lesComptes.add(new Compte(num, nom, solde));
   }

    @Override
public String toString() {
	return "ClientExterne [lesComptes=" + lesComptes + ", nom=" + nom + ", prenom=" + prenom + "]";
}   
   
}


