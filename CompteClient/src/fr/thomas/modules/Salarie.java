package fr.thomas.modules;

public abstract class Salarie {

    private double salaire;
    private String prenom, nom;

    public Salarie(int salaire, String prenom, String nom){
        this.salaire = salaire;
        this.prenom = prenom;
        this.nom = nom;
    }

    public double getSalaire() {
        return salaire;
    }

    public abstract void vererSalaire();

    public String getPrenom() {
        return prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setSalaire(int salaire) {
        this.salaire = salaire;
    }

    @Override
    public String toString() {
        return "Salaire: " + salaire + ", prénom: "+ prenom + ", nom: " + nom;
    }
}
