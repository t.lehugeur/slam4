package fr.thomas.modules;

import java.util.List;

public class ClientInterne extends Salarie implements Client{
	// liste des comptes du client
    private List<Compte> lesComptes;
    private double soldeCompte;

    public ClientInterne(int salaire, String prenom, String nom, double soldeCompte) {
        super(salaire, prenom, nom);
        this.soldeCompte = soldeCompte;
    }

    @Override
    public void vererSalaire() {
        this.soldeCompte = soldeCompte + getSalaire();
        System.out.println("Nouveau solde du compte: " + soldeCompte);
    }

    @Override
	public String toString() {
		return "ClientInterne [lesComptes=" + lesComptes + ", soldeCompte=" + soldeCompte + "]";
	}

	@Override
	public void creerCompte() {
		// TODO Auto-generated method stub
		System.out.println("compte créer ma belle");
		lesComptes.add(new Compte());
	}
    
    
}
