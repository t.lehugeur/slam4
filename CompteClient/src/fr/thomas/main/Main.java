package fr.thomas.main;

import fr.thomas.modules.ClientInterne;
import fr.thomas.modules.Employe;
import fr.thomas.modules.Salarie;
import fr.thomas.modules.Client;
import fr.thomas.modules.ClientExterne;

public class Main {

    public static void main(String[] args){
        Employe employe = new Employe(1000, "quentin", "pipelier", "N26");
        employe.vererSalaire();
        System.out.println(employe);

        ClientInterne client = new ClientInterne(1000, "quentin", "pipelier", 20);
        client.vererSalaire();
        System.out.println(client);

        Salarie employe2 = new Employe(10000000, "Kevin", "JSP", "Moi");
        System.out.println(employe2);
        
        Salarie S = new Employe(0, null, null, null);
        Salarie P = new ClientInterne(0, null, null, 0);
        Employe lol = new Employe(0, null, null, null);
        ClientInterne CI = new ClientInterne(0, null, null, 0);
        Client unC = new ClientInterne(0, null, null, 0);
        Client unCC = new ClientExterne();
        ClientExterne lolipop = new ClientExterne();

    }
}
