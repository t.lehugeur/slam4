<h1>Liste des restaurants</h1>

<?php
for ($i = 0; $i < count($listeRestos); $i++) {
    ?>
    <div class="card">
        <div class="descrCard"><?php echo "<a href='./?action=detail&idR=" . $listeRestos[$i]['id'] . "'>" . $listeRestos[$i]['nom'] . "</a>"; ?>
            <br />
            <?= $listeRestos[$i]["num_adr"] ?>
            <?= $listeRestos[$i]["voie_adr"] ?>
            <br />
            <?= $listeRestos[$i]["cp"] ?>
            <?= $listeRestos[$i]["ville"] ?>
        </div>
        <div class="tagCard">
            <ul id="tagFood">		
            </ul>
        </div>
    </div>
    <?php
}
?>
