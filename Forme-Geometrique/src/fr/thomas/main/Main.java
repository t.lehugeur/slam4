package fr.thomas.main;

import fr.thomas.modules.Cercle;
import fr.thomas.modules.Rectangle;
import fr.thomas.modules.Triangle;
import fr.thomas.modules.TriangleRectangle;

public class Main {
    public static void main (String[] args){
        Cercle cercle = new Cercle(10);
        System.out.println(cercle);

        Rectangle rectangle = new Rectangle(6.8, 3.2);
        System.out.println(rectangle);

        Triangle triangle = new TriangleRectangle(5, 7, 9);
        System.out.println(triangle);
    }
}
