package fr.thomas.modules;

public class TriangleRectangle extends Triangle {


    public TriangleRectangle(double cote1, double cote2, double cote3) {
        super(cote1, cote2, cote3);
    }

    @Override
    public double surface(){
        return cote1 * cote2/2;
    }

    @Override
    public String toString() {
        return "Trois côtés: "+ cote1 + ", " + cote2 + " et "+ cote3 +super.toString();
    }
}
