package fr.thomas.modules;

public abstract class Quadrilatere extends FigureGeo {

    protected double cote1, cote2;

    public Quadrilatere(double cote1, double cote2)
    {
        this.cote1 = cote1;
        this.cote2 = cote2;
    }

    @Override
    public double surface() {
        return cote1 * cote2;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
