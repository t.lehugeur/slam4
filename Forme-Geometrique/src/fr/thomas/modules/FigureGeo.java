package fr.thomas.modules;

public abstract class FigureGeo {
    // classe définissant les méthodes de calculs du périmètre et aire d'une figure géométrique quelconque

    // méthode abstraite de calcul du périmètre d'une forme géométrique
    public abstract double perimetre();

    // méthode abstraite de calcul de la surface d'une forme géométrique
    public abstract double surface();

    // récupère une chaîne contenant les caractéristiques d'une forme géométrique
    @Override
    public String toString()
    {
        return "\n- Périmètre = " + Math.round(perimetre()) + " - Surface = " + Math.round(surface());
    }
}

