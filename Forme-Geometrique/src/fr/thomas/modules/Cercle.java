package fr.thomas.modules;

public class Cercle extends FigureGeo{
// Cette classe est CONCRÈTE car toutes les méthodes sont implénmentées (réalisables)

    private double rayon;

    public Cercle(double rayon)
    {
        this.rayon = rayon;
    }


    @Override
    //calcul du périmètre
    public double perimetre() {
        return 2 * Math.PI * rayon;
    }

    @Override
    //calcul de la surface
    public double surface() {
        return Math.PI * rayon * rayon;
    }

    //affichage
    @Override
    public String toString()
    {
        return "Cercle de rayon : " + rayon + super.toString();
    }

}

