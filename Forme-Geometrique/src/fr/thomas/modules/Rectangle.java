package fr.thomas.modules;

public class Rectangle extends Quadrilatere{

    public Rectangle(double cote1, double cote2) {
        super(cote1, cote2);
    }

    @Override
    public double perimetre() {
        return (cote1 + cote2) * 2;
    }

    @Override
    public String toString() {
        return "Les deux côtés mesurent : "+cote1 + " et "+cote2 +super.toString();
    }
}
