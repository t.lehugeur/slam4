package fr.thomas.modules;

public abstract class Triangle extends FigureGeo{

    protected double cote1, cote2, cote3;

    public Triangle(double cote1, double cote2, double cote3){
        this.cote1 = cote1;
        this.cote2 = cote2;
        this.cote3 = cote3;
    }

    @Override
    public double perimetre() {
        return cote1 + cote2 + cote3;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
